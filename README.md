# check-disk-temp

This script will show the friendly name, serial number, bustype, firmware, temperature and the poweronhours of your disks which are mounted under a mountpath (directory) and are sorted by mountpath.

The accommodating powershell script is pretty static and does not (yet) accept parameters. This script was build so it would work on all my machines with different mountpaths but with a common part like 'Array'.

Here is a screenshot of the output:
![screenshot](images/screenshot.png)
